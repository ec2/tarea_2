/*
 *  Tarea 2
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 * 
 * Jorge Munoz Taylor
 * A53863
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>

#include <ctype.h> /*Include the function isdigit()*/
#include <ctime>

#define ADDRSIZE 32



/* Helper funtions */
void print_usage ()
{
  printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("\n The format for run the program is:\n");
  printf("\n   gunzip -c mcf.trace.gz | ./l1cache -t <#> -a <#> -l <#> -rp <#>");
  printf("\n\n -> The # is an int number");
  printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
  exit (0);
}



/* Print in console the console confguration */
void print_cache_configuration
(
  int _cache_size,
  int _associativity,
  int _block_size
)
{
  printf("\n____________________________________\n\n");
  printf("  Cache parameters:" );
  printf("\n____________________________________\n\n");
  printf("  Cache size (KB):           %d\n", _cache_size/1024);
  printf("  Cache Associativity:       %d\n", _associativity);
  printf("  Cache Block Size (bytes):  %d\n", _block_size);
  printf("____________________________________\n");
}; /* End of function */



/* Print in console the results of the simulation */
void print_statistics 
(
  float _overall_miss_rate,
  float _l1_read_miss_rate,
  int   _dirty_evictions,
  int   _l1_load_miss,
  int   _l1_store_miss,
  int   _l1_load_hits,
  int   _l1_store_hits
)
{
  printf("\n  Simulation results:             \n" );
  printf("____________________________________\n\n"); 
  printf("  Overall miss rate:         %.2f\n", _overall_miss_rate);
  printf("  Read miss rate:            %.2f\n", _l1_read_miss_rate);
  printf("  Dirty evictions:           %d\n"  , _dirty_evictions);
  printf("  Load misses:               %d\n"  , _l1_load_miss);
  printf("  Store misses:              %d\n"  , _l1_store_miss);
  printf("  Total misses:              %d\n"  , _l1_load_miss + _l1_store_miss);
  printf("  Load hits:                 %d\n"  , _l1_load_hits);
  printf("  Store hits:                %d\n"  , _l1_store_hits);
  printf("  Total hits:                %d\n"  , _l1_load_hits + _l1_store_hits);
  printf("____________________________________\n");
};/* End of function */



int main(int argc, char * argv []) {
  
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*               Program vars                  */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  int          _cache_size;
  int          _associativity;
  int          _block_size;
  int          _rp;
  bool         _arg_error = false;
  
  char         _hastag; 
  int          _ls;
  unsigned int _hex_address;
  long         _bin_address;
  int          _ic;

  int          tag_size           = 0;
  int          idx_size           = 0;
  int          offset_size        = 0;

  int          idx; /* address in binary of the SETS*/
  int          tag;
   
  float        _overall_miss_rate = 0;
  float        _l1_read_miss_rate = 0;
  int          _dirty_evictions   = 0;
  
  int          _l1_load_miss      = 0;
  int          _l1_store_miss     = 0;
  
  int         _l1_load_hits       = 0;
  int         _l1_store_hits      = 0;

  /* Struct define in the library time for store elapsed time */
  struct timespec t0, t1;
  struct operation_result operation_res = {};
  
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*                Parse arguments              */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  if(argc == 9) /*User NEED to pass 9 arguments*/
  {
    if( strcmp("-t", argv[1]) == 0 )
    {
      if( isdigit( *argv[2] ) != 0 ) _cache_size = 1024*atoi(argv[2]); /* In KB */
      else _arg_error = true;
    }
    else _arg_error = true;
  

    if(strcmp( "-a", argv[3]) == 0 )
    {
      if( isdigit( *argv[4] ) != 0 ) 
      {
        _associativity = atoi(argv[4]);

        if(_associativity == 0)
        {
          printf("\n-> Associativity cant be 0\n\n");
          return _arg_error = true;
        }
      }
      else _arg_error = true;
    }
    else _arg_error = true;
     

    if( strcmp("-l", argv[5]) == 0 )
    {
      if( isdigit( *argv[6]) != 0 ) _block_size = atoi(argv[6]);
      else _arg_error = true;
    }
    else _arg_error = true;
    
 
    if( strcmp("-rp", argv[7]) == 0 )
    {
      if( isdigit( *argv[8]) != 0 ) _rp = atoi(argv[8]);
      else _arg_error = true;
    } 
    else _arg_error = true;

  }
  /*User pass wrong number of arguments*/
  else _arg_error = true;

  /* If the user pass wrong args print a program usage */
  if(_arg_error == true) print_usage();
  

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*          Define L1 cache memory             */
  /*             and initialize it               */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  entry* L1_CACHE_BLOCK = new entry[ _cache_size/_block_size ];
  
  /* 
    Cache power up, initialize every block 
    _cache_size/_block_size is the number of blocks 
  */
  for( int i = 0; i < _cache_size/_block_size; i++)
  {
    L1_CACHE_BLOCK[i].valid    = 0;
    L1_CACHE_BLOCK[i].dirty    = false;

    if     ( _rp == 1 ) L1_CACHE_BLOCK[i].rp_value = 1;
    else if( _rp == 2 ) L1_CACHE_BLOCK[i].rp_value = (_associativity <= 2)? 0:2;
  
  }/* for end */
  
  /* Obtain the tag size, idx size and offset size in bits */
  field_size_get(
    _cache_size,
    _associativity,
    _block_size, 
    &tag_size,
    &idx_size,
    &offset_size
  );

  /* Print in terminal the actual cache configuration */
  print_cache_configuration
  (
    _cache_size,
    _associativity,
    _block_size
  );

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*               Program start                 */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  if( _rp == LRU) /* LRU */
  {
    clock_gettime( CLOCK_MONOTONIC, &t0 );
   
    while( scanf("%c %d %x %d\n", &_hastag, &_ls, &_hex_address, &_ic) != EOF )
    {
        /* Convert the unsigned int address to long */
        _bin_address = _hex_address | 0;
        
        address_tag_idx_get( 
          _bin_address, 
          tag_size, 
          idx_size, 
          offset_size, 
          idx, 
          tag);

        idx = idx*_associativity;

        /* Start LRU algorithm */
        lru_replacement_policy (
          idx,
          tag,
          _associativity,
          _ls,
          L1_CACHE_BLOCK,
          &operation_res,
          false);

          /* Read the operation_res struct and determine if there is a load or store miss/hit */
          if      ( operation_res.miss_hit == MISS_LOAD  ) _l1_load_miss++;
          else if ( operation_res.miss_hit == MISS_STORE ) _l1_store_miss++;
          else if ( operation_res.miss_hit == HIT_LOAD   ) _l1_load_hits++;
          else if ( operation_res.miss_hit == HIT_STORE  ) _l1_store_hits++;
          else return ERROR;

          /* Determine if there is an dirty eviction */
          if ( operation_res.dirty_eviction == true ) _dirty_evictions++;
    }/* While end */

    clock_gettime( CLOCK_MONOTONIC, &t1 );
  }/* if end */



  else if( _rp == NRU)
  {
    clock_gettime( CLOCK_MONOTONIC, &t0 );
   
    while( scanf("%c %d %x %d\n", &_hastag, &_ls, &_hex_address, &_ic) != EOF )
    {
        /* Convert the unsigned int address to long */
        _bin_address = _hex_address | 0;
        
        address_tag_idx_get( 
          _bin_address, 
          tag_size, 
          idx_size, 
          offset_size, 
          idx, 
          tag);

        idx = idx*_associativity;

        /* Start LRU algorithm */
        nru_replacement_policy (
          idx,
          tag,
          _associativity,
          _ls,
          L1_CACHE_BLOCK,
          &operation_res,
          false);

          /* Read the operation_res struct and determine if there is a load or store miss/hit */
          if      ( operation_res.miss_hit == MISS_LOAD  ) _l1_load_miss++;
          else if ( operation_res.miss_hit == MISS_STORE ) _l1_store_miss++;
          else if ( operation_res.miss_hit == HIT_LOAD   ) _l1_load_hits++;
          else if ( operation_res.miss_hit == HIT_STORE  ) _l1_store_hits++;
          else return ERROR;

          /* Determine if there is an dirty eviction */
          if ( operation_res.dirty_eviction == true ) _dirty_evictions++;
    }/* While end */

    clock_gettime( CLOCK_MONOTONIC, &t1 );
  }/* if end */
      


  else if( _rp == RRIP)
  {
    clock_gettime( CLOCK_MONOTONIC, &t0 );
   
    while( scanf("%c %d %x %d\n", &_hastag, &_ls, &_hex_address, &_ic) != EOF )
    {
        /* Convert the unsigned int address to long */
        _bin_address = _hex_address | 0;
        
        address_tag_idx_get( 
          _bin_address, 
          tag_size, 
          idx_size, 
          offset_size, 
          idx, 
          tag);

        idx = idx*_associativity;

        /* Start SRRIP algorithm */
        srrip_replacement_policy (
          idx,
          tag,
          _associativity,
          _ls,
          L1_CACHE_BLOCK,
          &operation_res,
          false);

          /* Read the operation_res struct and determine if there is a load or store miss/hit */
          if      ( operation_res.miss_hit == MISS_LOAD  ) _l1_load_miss++;
          else if ( operation_res.miss_hit == MISS_STORE ) _l1_store_miss++;
          else if ( operation_res.miss_hit == HIT_LOAD   ) _l1_load_hits++;
          else if ( operation_res.miss_hit == HIT_STORE  ) _l1_store_hits++;
          else return ERROR;

          /* Determine if there is an dirty eviction */
          if ( operation_res.dirty_eviction == true ) _dirty_evictions++;
    }/* While end */

    clock_gettime( CLOCK_MONOTONIC, &t1 );
  }/* if end */

  else
  {
    /* Error management */
    printf( "ERROR\n" );
    exit(0);
  }
      
  
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*           Print cache statistics            */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
 
  _l1_read_miss_rate = (float)_l1_load_miss/((float)_l1_load_miss+(float)_l1_store_miss+(float)_l1_load_hits+(float)_l1_store_hits);
  _overall_miss_rate = ((float)_l1_load_miss+(float)_l1_store_miss)/((float)_l1_load_miss+(float)_l1_store_miss+(float)_l1_load_hits+(float)_l1_store_hits);

  print_statistics(
   _overall_miss_rate,
   _l1_read_miss_rate,
   _dirty_evictions,

   _l1_load_miss,
   _l1_store_miss,
  
   _l1_load_hits,
   _l1_store_hits
  );
  

  /* Elapsed time for execute the algorithms */
  double time = (t1.tv_sec - t0.tv_sec);
  time += (t1.tv_nsec - t0.tv_nsec)/1000000000.0;
  printf( "\nExecution time: %f \n", time );
       
  return OK;

}