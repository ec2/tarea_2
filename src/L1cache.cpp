/*
 *  Tarea 2
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 * 
 * Jorge Munoz Taylor
 * A53863
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;



int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{
   /*Return the size of the index in bits*/
   *idx_size = log( cachesize_kb/(blocksize_bytes*associativity) )/log(2) ;
   
   /*Return the size of the offset in bits*/
   *offset_size = log(blocksize_bytes)/log(2);
   
  /*Return the size of the tag in bits*/
   *tag_size = ADDRSIZE - *idx_size - *offset_size;
   
   return OK;
};/* End of function */



void address_tag_idx_get(
   long address,
   int tag_size,
   int idx_size,
   int offset_size,
   int &idx,
   int &tag)
{
   long temp;

   temp = (pow( 2, idx_size ) - 1);
   
   temp = temp << offset_size;

   idx  = address & temp;

   idx  = idx >> offset_size;

   tag  = address >> ( idx_size + offset_size ) ; /* shift right */

   return;
};/* End of function */



int lru_replacement_policy ( int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{   

    if( associativity >= 1 )
    {
      int  CACHE_BLOCKS_TEMP [ associativity ] = {0};
      int  _valid_temp [ associativity ]       = {0};
      bool _dirty[ associativity ];
      bool _dirty_temp;
      result->dirty_eviction  = false;

      /*--------------------------------------*/
      /* LOOP FOR to verify if there is a HIT */
      /*--------------------------------------*/
      
      if( associativity > 1)
      {
         for( int i=0; i < associativity; i++ )
         {       
            /*--------------------------------------*/
            /*                 HIT                  */
            /*--------------------------------------*/
            if( cache_blocks[ idx + i].tag == tag && cache_blocks[ idx + i].valid == 1 )
            {
            _dirty_temp = cache_blocks[ idx + i].dirty;
            
            for( int j = 0; j < i; j++ )
            {
                  CACHE_BLOCKS_TEMP[j] = cache_blocks[ idx + j].tag; 
                  _dirty[j]            = cache_blocks[ idx + j].dirty;
            } /* for end */
               

            for( int j = 0; j < i; j++)
            {
                  cache_blocks[ idx + j+1].tag   = CACHE_BLOCKS_TEMP[j];
                  cache_blocks[ idx + j+1].dirty = _dirty[j];
            } /* for end */


            cache_blocks[idx].dirty = _dirty_temp;
            cache_blocks[idx].tag   = tag;
            

            if( loadstore == 0 )
            {
               result->miss_hit = HIT_LOAD;
            }/* if end */
            else
            {
                  cache_blocks[ idx ].dirty = true; 
                  result->miss_hit = HIT_STORE;
            }/* else end */

            i = associativity;/* Loop out */

            return OK;
            }/* if end */ 

         } /* For end */

      }/*if end*/
      
      else
      {
            /*--------------------------------------*/
            /*                 HIT                  */
            /*--------------------------------------*/
            if( cache_blocks[ idx ].tag == tag && cache_blocks[ idx ].valid == 1 )
            {            
               if( loadstore == 0 )
               {
                  result->miss_hit = HIT_LOAD;
               }/* if end */
               else
               {
                     cache_blocks[ idx ].dirty = true; 
                     result->miss_hit = HIT_STORE;
               }/* else end */

               return OK;
            }/* if end */ 

      }/* else if end */


      /*--------------------------------------*/
      /*                MISS                   /
      /*--------------------------------------*/
      if( cache_blocks[ idx + associativity-1].dirty == true && cache_blocks[ idx + associativity-1].valid == 1 )
      {
         result->dirty_eviction  = true;
         result->evicted_address = cache_blocks[associativity-1].tag;
      }/* if end */


      for( int i=0; i < associativity; i++ )
      {
         _dirty[i]            = cache_blocks[ idx + i ].dirty;
         CACHE_BLOCKS_TEMP[i] = cache_blocks[ idx + i ].tag;
         _valid_temp[i]       = cache_blocks[ idx + i ].valid;
      }/* for end */


      for ( int i = 0; i < associativity-1; i++)
      {
         cache_blocks[ idx + i+1 ].dirty = _dirty[i];
         cache_blocks[ idx + i+1 ].tag   = CACHE_BLOCKS_TEMP[i];
         cache_blocks[ idx + i+1 ].valid = _valid_temp[i];
      }/* for end */


      cache_blocks[ idx ].tag   = tag;
      cache_blocks[ idx ].valid = 1;


      if( loadstore == 0)
      {
         cache_blocks[ idx ].dirty = false;
         result->miss_hit          = MISS_LOAD;
         return OK;
      }/* if end */
      else
      {                
         cache_blocks[ idx ].dirty = true;
         result->miss_hit          = MISS_STORE;
         return OK;
      }/* else end */
 
    }/* if end*/
    else return PARAM;
    
    
    return ERROR;

}/* END OF FUNCTION */



int nru_replacement_policy ( int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   bool nru_bit_is_1       = false;
   result->dirty_eviction  = false;

   if( associativity < 1 ) return PARAM;

   /*--------------------------------------*/
   /* LOOP FOR to verify if there is a HIT */
   /*--------------------------------------*/

   if( associativity > 1)
   {
      for( int i=0; i < associativity; i++ )
      {       
         /*--------------------------------------*/
         /*                 HIT                  */
         /*--------------------------------------*/
         if( cache_blocks[ idx + i].tag == tag && cache_blocks[ idx + i].valid == 1 )
         {
            cache_blocks[ idx + i].rp_value = 0;

                  
            if( loadstore == 0 )
            {
                  result->miss_hit = HIT_LOAD;
            }/* if end */
            else
            {
                  cache_blocks[ idx + i].dirty = true; 
                  result->miss_hit             = HIT_STORE;      
            }/* else end */

            i = associativity;

            return OK;
         }/* if end */
      }/* for end */

   }/* if end */
   else
   {
         /*--------------------------------------*/
         /*                 HIT                  */
         /*--------------------------------------*/
         if( cache_blocks[ idx ].tag == tag && cache_blocks[ idx ].valid == 1 )
         {
            cache_blocks[ idx ].rp_value = 0;
   
            if( loadstore == 0 )
            {
                  result->miss_hit = HIT_LOAD;
            }/* if end */
            else
            {
                  cache_blocks[ idx ].dirty = true; 
                  result->miss_hit          = HIT_STORE;      
            }/* else end */

            return OK;
         }/* if end */
   }/* else end */

   /*--------------------------------------*/
   /*                  MISS                */
   /*--------------------------------------*/

   /*--------------------------------------*/
   /*   Search for 1 in the nru bits, if   */
   /*    didnt find it assign 1 to all     */
   /*         cache blocks nru bits         */
   /*--------------------------------------*/
   if( associativity > 1)
   {
      for( int i=0; i < associativity; i++ )
      {
         if( cache_blocks[ idx + i].rp_value == 1 )
         {
            nru_bit_is_1 = true;
            i            = associativity;
         }/* if end */
      }/* for end */

      if( nru_bit_is_1 == false )
      {
         for( int i=0; i < associativity; i++ ) cache_blocks[ idx + i].rp_value = 1;
      }/* if end */

      /*--------------------------------------*/
      for( int i=0; i< associativity; i++ )
      {
         if( cache_blocks[ idx + i].rp_value == 1 )
         {
            if( cache_blocks[ idx + i].dirty == true && cache_blocks[ idx + i].valid == 1 )
            {
               result->dirty_eviction  = true;
               result->evicted_address = cache_blocks[idx].tag;
            }/* if end */
            

            cache_blocks[ idx + i].tag      = tag;
            cache_blocks[ idx + i].valid    = 1;
            cache_blocks[ idx + i].rp_value = 0;


            if( loadstore == 0)
            {
               cache_blocks[ idx + i ].dirty = false;
               result->miss_hit = MISS_LOAD;
            }/* if end */
            else
            {                
               cache_blocks[ idx + i ].dirty = true;
               result->miss_hit = MISS_STORE;
            }/* else end */

            i = associativity;

            return OK;
         }/* if end */

      }/* for end */

   }/* if end */

   else
   {
      if( cache_blocks[ idx ].rp_value == 0 ) cache_blocks[ idx ].rp_value = 1;

      if( cache_blocks[ idx ].rp_value == 1 )
      {
         if( cache_blocks[ idx ].dirty == true && cache_blocks[ idx ].valid == 1 )
         {
            result->dirty_eviction  = true;
            result->evicted_address = cache_blocks[ idx ].tag;
         }/* if end */
            
         cache_blocks[ idx ].tag      = tag;
         cache_blocks[ idx ].valid    = 1;
         cache_blocks[ idx ].rp_value = 0;

         if( loadstore == 0)
         {
            cache_blocks[ idx ].dirty = false;
            result->miss_hit = MISS_LOAD;
         }/* if end */
         else
         {                
            cache_blocks[ idx ].dirty = true;
            result->miss_hit = MISS_STORE;
         }/* else end */

         return OK;
      }/* if end */
   
   }/* else end */

   return ERROR;

}/* END OF FUNCTION */



int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   bool    _there_are_rrpv_max   = false;
   uint8_t _rrpv = associativity <= 2? 0:2;
   result->dirty_eviction        = false;

   if( associativity < 1) return PARAM;

   /*--------------------------------------*/
   /*           Search for hit             */
   /*--------------------------------------*/
   if( associativity > 1)
   {
      for( int i=0; i<associativity; i++ )
      {
         if( cache_blocks[ idx + i ].tag == tag && cache_blocks[ idx + i ].valid == 1)
         {
            cache_blocks[ idx + i ].rp_value = 0;
               
            if( loadstore == 0 )
            {
               result->miss_hit = HIT_LOAD;
            }/* if end */
            else
            {
               cache_blocks[ idx + i].dirty = true; 
               result->miss_hit             = HIT_STORE;    
            }/* else end */

            i = associativity;

            return OK;
         }/* if end */

      }/* for end */
   
   }/* if end */
   else
   {
      if( cache_blocks[ idx ].tag == tag && cache_blocks[ idx ].valid == 1)
      {
         cache_blocks[ idx ].rp_value = 0;
               
         if( loadstore == 0 )
         {
            result->miss_hit = HIT_LOAD;
         }/* if end */
         else
         {
            cache_blocks[ idx ].dirty = true; 
            result->miss_hit          = HIT_STORE;    
         }/* else end */

         return OK;
      }/* if end */
   }/* else end */

   /*--------------------------------------*/
   /*                 MISS                 */
   /*--------------------------------------*/
   if( associativity > 1 )
   {
      while( _there_are_rrpv_max == false )
      {
         for( int i=0; i<associativity; i++ )
         {
            if( cache_blocks[ idx + i ].rp_value == 3 )
            {
               _there_are_rrpv_max = true;
               i = associativity;
            }/* if end */      
         }/* for end */


         if( _there_are_rrpv_max == false )
         {
            for( int i=0; i<associativity; i++ )
            {
               cache_blocks[ idx + i ].rp_value++;
            }/* for end */
         }/* if end */

      }/* while end */


      for( int i=0; i<associativity; i++ )
      {
         if( cache_blocks[ idx + i ].rp_value == 3 )
         {

            if( cache_blocks[ idx + i].dirty == true && cache_blocks[ idx + i].valid == 1 )
            {
               result->dirty_eviction  = true;
               result->evicted_address = cache_blocks[idx].tag;
            }/* if end */
            

            cache_blocks[ idx + i].tag      = tag;
            cache_blocks[ idx + i].valid    = 1;
            cache_blocks[ idx + i].rp_value = _rrpv;


            if( loadstore == 0)
            {
               cache_blocks[ idx + i ].dirty = false;
               result->miss_hit              = MISS_LOAD;
            }/* if end */
            else
            {                
               cache_blocks[ idx + i ].dirty = true;
               result->miss_hit              = MISS_STORE;
            }/* else end */

            i = associativity;

            return OK;
         }/* if end */

      }/* for end */

   }/* if end */
   else
   {
      while( _there_are_rrpv_max == false )
      {
         if( cache_blocks[ idx ].rp_value == 3 ) _there_are_rrpv_max = true;

         if( _there_are_rrpv_max == false ) cache_blocks[ idx ].rp_value++;
      }

      if( cache_blocks[ idx ].rp_value == 3 )
      {

         if( cache_blocks[ idx ].dirty == true && cache_blocks[ idx ].valid == 1 )
         {
            result->dirty_eviction  = true;
            result->evicted_address = cache_blocks[idx].tag;
         }/* if end */
            

         cache_blocks[ idx ].tag      = tag;
         cache_blocks[ idx ].valid    = 1;
         cache_blocks[ idx ].rp_value = _rrpv;


         if( loadstore == 0)
         {
            cache_blocks[ idx ].dirty = false;
            result->miss_hit          = MISS_LOAD;
         }/* if end */
         else
         {                
            cache_blocks[ idx ].dirty = true;
            result->miss_hit          = MISS_STORE;
         }/* else end */

         return OK;
      }/* if end */
      
         
   }/* else end */

   return ERROR;
}/* End of function */